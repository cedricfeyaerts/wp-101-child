<?php
function ajoute_mes_styles() {

    $parent_style = 'twentyseventeen-style'; // le template parent + '-style'

    // charge les styles du theme parent
    wp_enqueue_style( $parent_style, get_template_directory_uri() . '/style.css' ); 

    // charge les styles de ce theme ci
    wp_enqueue_style( 'child-style',
        get_stylesheet_directory_uri() . '/style.css', // ou se trouve mes style
        array( $parent_style ), // ce qui doit etre charge avant
        wp_get_theme()->get('Version') // ajoute un num de version pour le cache
    );
}
add_action( 'wp_enqueue_scripts', 'ajoute_mes_styles' );


// Ce fichier est charge avant le functions.php du theme parent
// On peut surcharger les fonctions
// Pour que ce theme enfant puisse etre aussi surchareable on definit les fonction comme ci:

if ( ! function_exists( 'theme_special_nav' ) ) {
    function theme_special_nav() {
        //  Do something.
    }
}
?>